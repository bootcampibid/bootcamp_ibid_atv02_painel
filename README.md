![Apex_1699066788339](https://github.com/zeldinha00/Web-Portfolio-Oficial/assets/14182590/f93b332e-d951-4ae3-9a83-0710a438fcf9)


# Projeto de Exemplo ASP.NET com Painéis de Formulário

Este é um projeto de exemplo em ASP.NET que demonstra o uso de painéis de formulário para coletar informações pessoais, de endereço e de login de um usuário. O projeto apresenta a funcionalidade de validação de formulário, redirecionamento de página e exibição dos dados preenchidos em uma página separada.

## Funcionalidades Implementadas

- Coleta de informações pessoais, incluindo nome, sobrenome, gênero e número de celular.
- Coleta de informações de endereço, incluindo endereço, cidade e CEP.
- Coleta de informações de login, incluindo nome de usuário e senha.
- Validação de formulário para garantir que todos os campos sejam preenchidos antes de avançar para o próximo painel.

## Arquivos Importantes

- `PaginaPainel.aspx`: Página principal que contém os painéis de formulário e a lógica de validação.
- `styles.css`: Arquivo CSS para estilizar a apresentação da página.

## Como Executar o Projeto

1. Clone o repositório para o seu ambiente local.
2. Abra o projeto em uma IDE compatível com ASP.NET.
3. Execute o projeto em um servidor web local.
4. Preencha os formulários com os dados solicitados e siga as instruções na página.

## Contribuição

Este projeto foi criado como um exemplo educacional **IBID** para demonstrar o uso de painéis de formulário em ASP.NET. Contribuições são bem-vindas para melhorias e aprimoramentos adicionais.

## Licença

Este projeto é licenciado sob a licença MIT - consulte o arquivo [LICENSE.md](LICENSE.md) para obter mais detalhes.

