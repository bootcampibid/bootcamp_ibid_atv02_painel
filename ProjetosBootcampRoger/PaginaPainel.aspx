﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaPainel.aspx.cs" Inherits="ProjetosBootcampRoger.PaginaPainel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Pagina Painel</title>

    <!-- Estilização do CSS -->
    <link rel="stylesheet" type="text/css" href="styles.css"/>

    <style type="text/css">
        .auto-style1 {
            width: 100px;
            height: 40px;
        }
        .auto-style2 {
            height: 40px;
        }
        .auto-style3 {
            height: 50px;
        }
        .auto-style4 {
            margin: 50px auto;
            text-align: left;
            width: 320px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <div class="centralizar_div ">
             <table style="width:100%;">
            <tr>
                <td  colspan="3" style="text-align:left; text-align:center">
                    <strong><span style="font-size:18pt; color: #b6ff00">Exemplo do controle Panel em ASP.NET C#</span></strong>
                </td>
            </tr>
            <tr>
                <td style="width:100%">
                    <!-- PAINEL DE PAI -->
                    <asp:Panel ID="Panel1" runat="server" >

                        <!-- PAINEL DE DADOS PESSOAIS -->
                        <asp:Panel ID="Panel2" runat="server">
                            <table class="auto-style4">
                                <tr>
                                    <td class="container_info" colspan="2" style="text-align:center; border-bottom:#b6ff00 thin solid">
                                        <strong>
                                            <span>
                                                Informações Pessoais
                                            </span>
                                        </strong>
                                    </td>
                                </tr>
                                 <!-- Campo Nome-->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Nome</td>
                                    <td class="auto-style2" >
                                        <asp:TextBox ID="txtNome" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo Sobrenome -->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Sobrenome</td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="txtSobronome" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo Gênero -->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Gênero</td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="txtGenero" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo Celula r-->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Celular</td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="txtNumero" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo Butão Próximo -->
                                <tr>
                                    <td colspan="2" style="text-align: center" class="auto-style1"> 
                                        <asp:Button class="myButtonVoltar" ID="btnAvancaEndereco" runat="server" Width="131px" Text="Próximo ▶" OnClick="btnAvancaEndereco_Click"/>
                                    </td>
                                </tr>
                                <!-- Campo Menssagem de ERRO -->
                                <tr >
                                    <td colspan="2" style="text-align: center; color:#ff0000" class="auto-style3">
                                        <asp:Label ID="lblErrorNaTela" runat="server" ForeColor="#ff0000"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <!-- PAINEL DE ENDEREÇO -->
                        <asp:Panel ID="Panel3" runat="server" Visible="false">
                            <table class="auto-style4">
                                 <tr>
                                    <td class="container_info" colspan="2" style="text-align:center; border-bottom:#b6ff00 thin solid">
                                        <strong>
                                            <span>
                                                Detalhes do Endereço
                                            </span>
                                        </strong>
                                    </td>
                                </tr>
                                <!-- Campo Endereço -->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Endereço</td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="txtEndereco" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo Cidade -->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Cidade</td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="txtCidade" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo CEP -->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">CEP</td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="txtCEP" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo Butão VOLTAR / PRÓXIMO -->
                                <tr>
                                    <td style="text-align: left" class="auto-style1"> 
                                        <asp:Button class="myButtonVoltar" ID="btnVoltarInfoPessoais" runat="server" Width="111px" Text="◀ Voltar" OnClick="btnVoltarInfoPessoais_Click" />
                                    </td>
                                    <td style="text-align: right" class="auto-style1">
                                        <asp:Button class="myButtonVoltar" ID="btnAvanacaLogin" runat="server" Width="123px" Text="Próximo ▶" OnClick="btnAvanacaLogin_Click" />
                                    </td>
                                </tr>
                                <!-- Campo Menssagem de ERRO -->
                                <tr >
                                    <td colspan="2" style="text-align: center; color:#ff0000" class="auto-style3">
                                        <asp:Label ID="lblErrorNaTelaEndereco" runat="server" ForeColor="#ff0000"></asp:Label>
                                    </td>
                                </tr>


                            </table>
                        </asp:Panel>

                        <!-- PAINEL DE LOGIN -->
                        <asp:Panel ID="Panel4" runat="server" Visible="false">
                            <table class="auto-style4">
                                 <tr>
                                    <td class="container_info" colspan="2" style="text-align:center; border-bottom:#b6ff00 thin solid">
                                        <strong>
                                            <span>
                                                Área de Login
                                            </span>
                                        </strong>
                                    </td>
                                </tr>
                                <!-- Campo LOGIN -->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Login</td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="txtLogin" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo SENHA -->
                                <tr class="container_linhas">
                                    <td style="text-align:center;" class="auto-style1">Senha</td>
                                    <td class="auto-style2">
                                    <asp:TextBox ID="txtSenha" runat="server" BackColor="#CCCCCC" BorderColor="White" Font-Bold="False" Height="30px" TextMode="Password" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <!-- Campo butão VOLTAR / ENVIAR -->
                                <tr>
                                    <td style="text-align: left" class="auto-style1"> 
                                        <asp:Button class="myButtonVoltar" ID="btnVoltarEndereco" runat="server" Width="112px" Text="◀ Voltar" OnClick="btnVoltarEndereco_Click" />
                                    </td>
                                    <td style="text-align: right" class="auto-style1"> 
                                        <asp:Button class="myButton" ID="btnEnviar" runat="server" Width="98px" Text="Enviar" OnClick="btnEnviar_Click1" />
                                    </td>
                                </tr>
                                <!-- Campo Menssagem de ERRO -->
                                <tr >
                                    <td colspan="2" style="text-align: center; color:#ff0000" class="auto-style3">
                                        <asp:Label ID="lblErrorNaTelaLogin" runat="server" ForeColor="#ff0000"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <!-- Painel do que foi cadastrado -->
                        <asp:Panel ID="Panel5" runat="server" CssClass="result-panel" Visible="false">
                            <h2>Dados informado com sucesso! ✅</h2>
                            <div class="result-container">
                                <div><strong>Nome:</strong> <span id="lblNomeDigitado" runat="server"></span></div>
                                <div><strong>Gênero:</strong> <span id="lblGeneroDigitado" runat="server"></span></div>
                                <div><strong>Celular:</strong> <span id="lblCelularDigitado" runat="server"></span></div>
                                <div><strong>Endereço:</strong> <span id="lblEnderecoDigitado" runat="server"></span></div>
                                <div><strong>Cidade:</strong> <span id="lblCidadeDigitado" runat="server"></span></div>
                                <div><strong>CEP:</strong> <span id="lblCepDigitado" runat="server"></span></div>
                                <div><strong>Login:</strong> <span id="lblLoginDigitado" runat="server"></span></div>
                                <div><strong>Senha:</strong> <span id="lblSenhaDigitado" runat="server"></span></div>
                            </div>
                            <p class="thanks-message">Obrigado por usar o sistema IBID 💚</p>
                        </asp:Panel>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>
