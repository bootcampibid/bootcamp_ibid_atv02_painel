﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjetosBootcampRoger
{
    public partial class PaginaPainel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void btnAvancaEndereco_Click(object sender, EventArgs e)
        {   
            // Validar se todos campos estão preenchidos 
            if (string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtSobronome.Text) || string.IsNullOrEmpty(txtGenero.Text) || string.IsNullOrEmpty(txtNumero.Text))
            {
                lblErrorNaTela.Text = "❌ Preencha todos os campos";
                return;
            }

            // Validar se os campos de Nome, Sobrenome e Gênero não estão vazios
            //
            Regex regexNome = new Regex("^[a-zA-Z ]+$");
            if (!regexNome.IsMatch(txtNome.Text))
            {
                lblErrorNaTela.Text = "❌ O campo NOME deve conter apenas letras.";
                return;
            }

            if (!regexNome.IsMatch(txtSobronome.Text))
            {
                lblErrorNaTela.Text = "❌ O campo SOBRENOME deve conter apenas letras.";
                return;
            }

            // Validar se o número de celular tem exatamente 11 dígitos
            if (txtNumero.Text.Length != 11)
            {
                lblErrorNaTela.Text = "❌ O número de CELULAR deve ter exatamente 11 dígitos.";
                return;
            }

            // Validar se o campo celular contém apenas números

            //  ^: Representa o início da string.
            // [0 - 9]: Representa a faixa de caracteres de 0 a 9, o que significa que aceita qualquer dígito de 0 a 9.
            // +: Indica que o dígito de 0 a 9 pode ocorrer uma ou mais vezes na string.
            // $: Representa o final da string.

            Regex regex = new Regex(@"^[0-9]+$");
            if (!regex.IsMatch(txtNumero.Text))
            {
                lblErrorNaTela.Text = "❌ O campo CELULAR deve conter apenas números.";
                return;
            }

            //// Formatando o número de celular
            string celular = txtNumero.Text;
            celular = string.Format("({0}) {1}-{2}", celular.Substring(0, 2), celular.Substring(2, 5), celular.Substring(7));

            // Ação a ser executada se todos os campos estiverem preenchidos corretamente
            Panel2.Visible = false;
            Panel3.Visible = true;
            Panel4.Visible = false;
        }

        // TRATATIVA BOTÃO VOLTAR AS INFORMAÇÕES PESSOAIS
        protected void btnVoltarInfoPessoais_Click(object sender, EventArgs e)
        {
            Panel2.Visible = true;
            Panel3.Visible = false;
            Panel4.Visible = false;
        }

        // TRATATIVA ENDEREÇO
        protected void btnAvanacaLogin_Click(object sender, EventArgs e)
        {
            // Validar se os campos estão todos preenchidos 
            if (string.IsNullOrEmpty(txtEndereco.Text) || string.IsNullOrEmpty(txtCidade.Text) || string.IsNullOrEmpty(txtCEP.Text))
            {
                lblErrorNaTelaEndereco.Text = "❌ Preencha todos os campos";
                return;
            }

            // Validar o campo Cidade para ter apenas String ( AFABETICO )
            Regex regexCidade = new Regex("^[a-zA-Z ]+$");
            if (!regexCidade.IsMatch(txtCidade.Text))
            {
                lblErrorNaTelaEndereco.Text = "❌ O campo CIDADE deve conter apenas letras.";
                return;
            }

            // Validar se os celular tem apenas números
            Regex regex = new Regex(@"^[0-9]+$");
            if (!regex.IsMatch(txtCEP.Text))
            {
                lblErrorNaTelaEndereco.Text = "❌ O campo CEP deve conter apenas números.";
                return;
            }

            // Validar se o número de celular tem exatamente 11 dígitos
            if (txtCEP.Text.Length != 8)
            {
                lblErrorNaTelaEndereco.Text = "❌ O número de CEP deve ter exatamente 08 dígitos.";
                return;
            }
            // Ação a ser executada se todos os campos estiverem preenchidos corretamente
            Panel2.Visible = false;
            Panel3.Visible = false;
            Panel4.Visible = true;
        }

        // // TRATATIVA BOTÃO VOLTAR AS ENDEREÇO
        protected void btnVoltarEndereco_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel3.Visible = true;
            Panel4.Visible = false;
        }

        // TELA QUE MOSTRA OS RESULTADOS DIGITADOS 
        protected void btnEnviar_Click1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLogin.Text) || string.IsNullOrEmpty(txtSenha.Text))
            {
                // ALERT EM JAVASCRIPT
                //Response.Write("<script>alert('Por favor, preencha todos os campos antes de prosseguir.');</script>");

                lblErrorNaTelaLogin.Text = "❌ Preencha todos os campos";
                return;
            }

            lblNomeDigitado.InnerText = $"{txtNome.Text} {txtSobronome.Text}"; 
            lblGeneroDigitado.InnerText = txtGenero.Text;
            lblCelularDigitado.InnerText = txtNumero.Text;
            lblEnderecoDigitado.InnerText = txtEndereco.Text; 
            lblCidadeDigitado.InnerText = txtCidade.Text; 
            lblCepDigitado.InnerText = txtCEP.Text; 
            lblLoginDigitado.InnerText = txtLogin.Text; 
            lblSenhaDigitado.InnerText = "********"; // Não exibimos senhas

            // Ocultando os painéis anteriores e exibindo Panel5
            Panel2.Visible = false;
            Panel3.Visible = false;
            Panel4.Visible = false;
            Panel5.Visible = true;
        }
    }
}